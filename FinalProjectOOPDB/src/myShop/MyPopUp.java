package myShop;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class MyPopUp extends JPopupMenu {
	
	private JMenuItem delItem = new JMenuItem("Delete One");
	private JMenuItem delItems = new JMenuItem("Delete All");
	private BaseRepository repo = new BaseRepository();
	
	public MyPopUp() {
		this.add(delItem);
		delItem.addActionListener(new DeleteOneInstance());
		this.add(delItems);
		delItems.addActionListener(new DeleteAllInstances());
	}
	
	class DeleteOneInstance implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			User u = MainClass.shopFrame.loggedUser;
			int row = MainClass.shopFrame.table.getSelectedRow();
			int column = 0;
			String prod_name = MainClass.shopFrame.table.getValueAt(row, column).toString();
			int prod_Id = repo.GetItemId(prod_name);
			repo.DeleteSell(prod_Id, 1);
			//MainClass.shopFrame.table.setModel(repo.eachPersonTable(u));
			u.setRevenue(repo.CalculaterUserTotalRevenue(u));
			MainClass.shopFrame.PopulateRevenue(u);
		}
	}
	class DeleteAllInstances implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			User u = MainClass.shopFrame.loggedUser;
			int row = MainClass.shopFrame.table.getSelectedRow();
			int column = 0;
			String prod_name = MainClass.shopFrame.table.getValueAt(row, column).toString();
			int prod_Id = repo.GetItemId(prod_name);
			column = 2;
			int count = Integer.parseInt(MainClass.shopFrame.table.getValueAt(row, column).toString());
			repo.DeleteSell(prod_Id, count);
			u.setRevenue(repo.CalculaterUserTotalRevenue(u));
			MainClass.shopFrame.PopulateRevenue(u);
		}

		
		
	}
}

