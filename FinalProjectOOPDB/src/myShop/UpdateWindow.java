package myShop;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class UpdateWindow {

	private JFrame frmUpdateWindow;
	protected JTextField fName_field;
	protected JTextField lName_Field;
	protected JTextField uName_field;
	private BaseRepository repo = new BaseRepository();
	private JPasswordField password_field;
	private JPasswordField password_field_confirm;

	
	public UpdateWindow() {
		initialize();
		frmUpdateWindow.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmUpdateWindow = new JFrame();
		frmUpdateWindow.setTitle("Update Window");
		frmUpdateWindow.setBounds(100, 100, 374, 215);
		frmUpdateWindow.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmUpdateWindow.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("First Name");
		lblNewLabel.setBounds(10, 22, 73, 13);
		frmUpdateWindow.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Last Name");
		lblNewLabel_1.setBounds(10, 46, 73, 14);
		frmUpdateWindow.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New password");
		lblNewLabel_2.setBounds(10, 96, 103, 14);
		frmUpdateWindow.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Username");
		lblNewLabel_3.setBounds(10, 71, 73, 14);
		frmUpdateWindow.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Confirm password");
		lblNewLabel_4.setBounds(10, 121, 103, 14);
		frmUpdateWindow.getContentPane().add(lblNewLabel_4);
		
		fName_field = new JTextField();
		fName_field.setBounds(123, 18, 190, 20);
		frmUpdateWindow.getContentPane().add(fName_field);
		fName_field.setColumns(10);
		
		lName_Field = new JTextField();
		lName_Field.setColumns(10);
		lName_Field.setBounds(123, 43, 190, 20);
		frmUpdateWindow.getContentPane().add(lName_Field);
		
		uName_field = new JTextField();
		uName_field.setColumns(10);
		uName_field.setBounds(123, 68, 190, 20);
		frmUpdateWindow.getContentPane().add(uName_field);
		
		JButton updateButton = new JButton("Update");
		updateButton.setBounds(133, 142, 89, 23);
		frmUpdateWindow.getContentPane().add(updateButton);
		
		password_field = new JPasswordField();
		password_field.setBounds(123, 93, 190, 20);
		frmUpdateWindow.getContentPane().add(password_field);
		
		password_field_confirm = new JPasswordField();
		password_field_confirm.setBounds(123, 118, 190, 20);
		frmUpdateWindow.getContentPane().add(password_field_confirm);
		updateButton.addActionListener(new UpdateAction());
	}
	class UpdateAction implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			User user = MainClass.shopFrame.loggedUser;
			String username_ = "";
			String password_ = "";
			if(password_field.getText().length() == 0 || fName_field.getText().length() == 0 || lName_Field.getText().length() == 0 || 
					uName_field.getText().length() ==  0) {
				repo.infoBox("Please enter all the fields.", "Oops!");
				return;
			}
				
			if(password_field.getText().equals(password_field_confirm.getText())) {
				
			username_ = uName_field.getText();
			password_ = password_field.getText();
			repo.UpdateUser(user,fName_field.getText() , lName_Field.getText(), uName_field.getText(), password_field.getText());
			}
			else {
				repo.infoBox("Passwords don't match", "Oops!");
				return;
			}
			
			user = repo.GetFirstUserOrDefault(username_, password_);
			user.setRevenue(repo.CalculaterUserTotalRevenue(user));
			MainClass.shopFrame.PopulateRevenue(user);
		}
	}
}
