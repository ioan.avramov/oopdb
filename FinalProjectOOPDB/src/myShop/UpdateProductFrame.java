package myShop;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UpdateProductFrame {

	private JFrame updateProductFrame;
	protected JTextField productName_field;
	protected JTextField productPrice_field;
	private BaseRepository repo = new BaseRepository();


	/**
	 * Create the application.
	 */
	public UpdateProductFrame() {
		initialize();
		updateProductFrame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		updateProductFrame = new JFrame();
		updateProductFrame.setTitle("Update Window");
		updateProductFrame.setBounds(100, 100, 241, 145);
		updateProductFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		updateProductFrame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Product name: ");
		lblNewLabel.setBounds(10, 11, 77, 14);
		updateProductFrame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Product Price: ");
		lblNewLabel_1.setBounds(10, 36, 77, 14);
		updateProductFrame.getContentPane().add(lblNewLabel_1);
		
		productName_field = new JTextField();
		productName_field.setBounds(97, 8, 118, 20);
		updateProductFrame.getContentPane().add(productName_field);
		productName_field.setColumns(10);
		
		productPrice_field = new JTextField();
		productPrice_field.setBounds(97, 33, 59, 20);
		updateProductFrame.getContentPane().add(productPrice_field);
		productPrice_field.setColumns(10);
		
		JButton update_Btn = new JButton("Update");
		update_Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Product prod = MainClass.shopFrame.selectedProduct;
				double new_Price;
				try {
				new_Price = Double.parseDouble(productPrice_field.getText());
				
				repo.UpdateProduct(prod, productName_field.getText(), new_Price);
				
				MainClass.shopFrame.selectedProduct = repo.SetSelectedProduct(productName_field.getText());
				MainClass.shopFrame.productsCombo.removeAllItems();
				MainClass.shopFrame.SetComboBox();
				
				User u = MainClass.shopFrame.loggedUser;
				MainClass.shopFrame.table.setModel(repo.eachPersonTable(u));
				
				}
				catch(Exception ex){
					repo.infoBox("Invalid entry!", "You piece of shit11!");
				}
			}
		});
		update_Btn.setBounds(67, 63, 89, 23);
		updateProductFrame.getContentPane().add(update_Btn);
	}

}
