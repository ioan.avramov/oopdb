package myShop;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class AddProductFrame {

	private JFrame frmAddProduct;
	private JTextField productNameField;
	private JTextField productPriceField;
	private BaseRepository repo = new BaseRepository();
	
	/** 
	 * Launch the application.
	 */
	public void launch() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddProductFrame window = new AddProductFrame();
					window.frmAddProduct.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddProductFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAddProduct = new JFrame();
		frmAddProduct.setTitle("Add Product");
		frmAddProduct.setBounds(100, 100, 328, 210);
		frmAddProduct.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmAddProduct.getContentPane().setLayout(null);
		
		JLabel productName = new JLabel("Name: ");
		productName.setBounds(10, 30, 46, 14);
		frmAddProduct.getContentPane().add(productName);
		
		JLabel productPrice = new JLabel("Price: ");
		productPrice.setBounds(10, 65, 46, 14);
		frmAddProduct.getContentPane().add(productPrice);
		
		productNameField = new JTextField();
		productNameField.setBounds(66, 27, 178, 20);
		frmAddProduct.getContentPane().add(productNameField);
		productNameField.setColumns(10);
		
		productPriceField = new JTextField();
		productPriceField.setColumns(10);
		productPriceField.setBounds(66, 62, 178, 20);
		frmAddProduct.getContentPane().add(productPriceField);
		
		JButton addBtn = new JButton("Add");
		addBtn.setBounds(66, 93, 89, 23);
		frmAddProduct.getContentPane().add(addBtn);
		addBtn.addActionListener(new AddProduct());
	}
	
	class AddProduct implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			float price;
			
			if(productNameField.getText().length() == 0 && productPriceField.getText().length() == 0)
			{
				repo.infoBox("Please insert product's properties", "Just Do It");
				return;
			}
			
			try {
				price = Float.parseFloat(productPriceField.getText());
				repo.InsertProduct(productNameField.getText(), price);
				MainClass.shopFrame.SetComboBox();
				repo.infoBox("Product Added", "Nice!");
				productNameField.setText("");
				productPriceField.setText("");
			}
			catch(Exception ex){
				repo.infoBox("Invalid Price", "Oops!");
			}
			
			
		}
		
	}
}
