package myShop;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;

import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import com.jgoodies.forms.layout.FormSpecs;
import java.awt.GridBagLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;

import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JCheckBox;
import javax.swing.JDesktopPane;
import javax.swing.JLayeredPane;

public class shopFrame {

	public JFrame frmMyShop;
	protected JTextField idField;
	protected JTextField usernameField;
	protected JTextField nameField;
	protected JTextField priceField;
	protected JTextField quantityField;
	protected JTextField usernameLogInField;
	private JPasswordField passwordLogInField;
	protected JTable table;
	protected JTextField revenueField;
	private JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private JButton loginBtn = new JButton("Log in");
	private JButton regButton = new JButton("Register");
	private JButton addProductBtn = new JButton("Add Product");
	private JButton btnSell = new JButton("Sell");
	protected JComboBox<String> productsCombo = new JComboBox<String>();
	public User loggedUser;
	public Product selectedProduct;
	private BaseRepository repo = new BaseRepository();

	public shopFrame() {
		initialize();
		 
	}

	private void initialize() {

	//MAIN FRAME	
		frmMyShop = new JFrame();
		frmMyShop.setTitle("My Shop");
		frmMyShop.setSize(450, 346);
		frmMyShop.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMyShop.getContentPane().setLayout(null);
	//-- MAIN FRAME
		
	//TABBS
		
		tabbedPane.setBounds(0, 0, 434, 311);
		frmMyShop.getContentPane().add(tabbedPane);
	//--TABBS
		JPanel loginPanel = new JPanel();
		tabbedPane.addTab("Log in", null, loginPanel, null);
		loginPanel.setLayout(null);
		
		
	//LOG IN TAB
		JLabel loginUserLabel = new JLabel("Username: ");
		loginUserLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		loginUserLabel.setBounds(12, 34, 67, 14);
		loginPanel.add(loginUserLabel);
		
		JLabel loginPassLabel = new JLabel(" Password:  ");
		loginPassLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		loginPassLabel.setBounds(12, 79, 83, 14);
		loginPanel.add(loginPassLabel);
		
		usernameLogInField = new JTextField();
		usernameLogInField.setBounds(110, 34, 247, 18);
		loginPanel.add(usernameLogInField);
		usernameLogInField.setColumns(10);
		
		passwordLogInField = new JPasswordField();
		passwordLogInField.setBounds(110, 79, 247, 18);
		loginPanel.add(passwordLogInField);
		
		
		loginBtn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		loginBtn.setActionCommand("");
		loginBtn.setBounds(110, 110, 89, 23);
		loginPanel.add(loginBtn);
		loginBtn.addActionListener(new LogInAction());
		
		
		regButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		regButton.setActionCommand("");
		regButton.setBounds(223, 110, 89, 23);
		loginPanel.add(regButton);
		regButton.addActionListener(new AddAction());
		//-- LOG IN TAB
			
		//REVENUE PANEL
			JPanel RevPanel = new JPanel();
			tabbedPane.addTab("Revenue", null, RevPanel, null);
			RevPanel.setLayout(null);
			
			JLabel idLabel = new JLabel("ID");
			idLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			idLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
			
			idLabel.setBounds(27, 11, 70, 30);
			RevPanel.add(idLabel);
			
			JLabel nameLabel = new JLabel("Full name");
			nameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			nameLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
			nameLabel.setBounds(27, 52, 70, 30);
			RevPanel.add(nameLabel);
			
			JLabel usernameLabel = new JLabel("Username");
			usernameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			usernameLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
			usernameLabel.setBounds(27, 93, 70, 30);
			RevPanel.add(usernameLabel);
			
			idField = new JTextField();
			idField.setBounds(126, 17, 20, 20);
			RevPanel.add(idField);
			idField.setColumns(10);
			idField.setEditable(false);
			
			
			usernameField = new JTextField();
			usernameField.setBounds(127, 99, 200, 20);
			RevPanel.add(usernameField);
			usernameField.setColumns(10);
			usernameField.setEditable(false);
			
			nameField = new JTextField();
			nameField.setColumns(10);
			nameField.setBounds(127, 58, 200, 20);
			RevPanel.add(nameField);
			RevPanel.setVisible(false);
			nameField.setEditable(false);
			
			table = new JTable();
			table.setBounds(27, 148, 302, 124);
			RevPanel.add(table);
			table.addMouseListener(new TableMouseClick());
			
			JLabel lblRevenue = new JLabel("Revenue");
			lblRevenue.setBounds(339, 188, 51, 20);
			RevPanel.add(lblRevenue);
			
			revenueField = new JTextField();
			revenueField.setEditable(false);
			revenueField.setBounds(339, 211, 70, 20);
			RevPanel.add(revenueField);
			revenueField.setColumns(10);
			
			JLabel lblProductName = new JLabel("Product name      |        Price               |        Qantity sold");
			lblProductName.setBounds(27, 134, 300, 14);
			RevPanel.add(lblProductName);
			
			JButton updateBtn = new JButton("Update User");
			updateBtn.setBounds(156, 16, 105, 23);
			RevPanel.add(updateBtn);
			
			JButton deleteUserBtn = new JButton("Delete");
			deleteUserBtn.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					if(loggedUser != null) {
						repo.DeleteUser(loggedUser);
						Logout(frmMyShop);
					}
				}
			});
			deleteUserBtn.setBounds(271, 16, 105, 23);
			RevPanel.add(deleteUserBtn);
			
			JButton LogoutBtn = new JButton("Log out");
			LogoutBtn.setFont(new Font("Tahoma", Font.PLAIN, 10));
			LogoutBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Logout(frmMyShop);
				}
			});
			LogoutBtn.setBounds(339, 242, 70, 23);
			RevPanel.add(LogoutBtn);
			updateBtn.addActionListener(new OpenUpdateWindow());
	//-- REVENUE PANEL	
		
	//SELLS PANEL
		JPanel SellsPanel = new JPanel();
		tabbedPane.addTab("Sells", null, SellsPanel, null);
		SellsPanel.setLayout(null);
		
		JLabel productNameLabel = new JLabel("Product: ");
		productNameLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		productNameLabel.setBounds(10, 11, 57, 23);
		SellsPanel.add(productNameLabel);
		
		JLabel productPriceLabel = new JLabel("Price: ");
		productPriceLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		productPriceLabel.setBounds(10, 45, 57, 23);
		SellsPanel.add(productPriceLabel);
		
		JLabel productQuantityLabel = new JLabel("Quantity");
		productQuantityLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		productQuantityLabel.setBounds(10, 79, 57, 23);
		SellsPanel.add(productQuantityLabel);
		
	    //Combo Box
		productsCombo.setBounds(77, 13, 215, 20);
		SellsPanel.add(productsCombo);
		productsCombo.addItemListener(new ItemChangeListener());
		
		priceField = new JTextField();
		priceField.setEditable(false);
		priceField.setText("");
		priceField.setBounds(77, 47, 71, 20);
		SellsPanel.add(priceField);
		priceField.setColumns(10);
		
		quantityField = new JTextField();
		quantityField.setBounds(77, 81, 71, 20);
		SellsPanel.add(quantityField);
		quantityField.setColumns(10);
		
		
		addProductBtn.setBounds(302, 12, 117, 23);
		SellsPanel.add(addProductBtn);
		addProductBtn.addActionListener(new AddProductAction());
		
		
		btnSell.setBounds(77, 112, 71, 23);
		SellsPanel.add(btnSell);
		btnSell.addActionListener(new SellItemAction());
		
		JButton update_btn = new JButton("Update");
		update_btn.setFont(new Font("Tahoma", Font.PLAIN, 11));
		update_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UpdateProductFrame upf = new UpdateProductFrame();
				upf.productName_field.setText(productsCombo.getSelectedItem().toString());
				upf.productPrice_field.setText(priceField.getText());
				
			}
		});
		update_btn.setBounds(302, 46, 117, 23);
		SellsPanel.add(update_btn);
		
		JButton deleteProdcut_btn = new JButton("Delete");
		deleteProdcut_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				repo.DeleteProduct(selectedProduct);
				productsCombo.removeAllItems();
				SetComboBox();
			}
		});
		deleteProdcut_btn.setBounds(302, 80, 117, 23);
		SellsPanel.add(deleteProdcut_btn);
		
		
	//-- SELLS PANEL	
		
	}
	class OpenUpdateWindow implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			UpdateWindow upWin = new UpdateWindow();
			
			upWin.fName_field.setText(loggedUser.getFirstName());
			upWin.lName_Field.setText(loggedUser.getLastName());
			upWin.uName_field.setText(loggedUser.getUsername());
		}
		
	}
	
	class SellItemAction implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int quantity = 0;
			if(quantityField.getText().length() == 0) {
				repo.infoBox("Insert quantity.", "Pssst..");
				return;
			}
			try {
				quantity = Integer.parseInt(quantityField.getText());
				
			}catch(Exception e){
				repo.infoBox("Invalid value.", "What the fuck are you doing?");
				return;
			}
			repo.SellItem(selectedProduct, quantity, loggedUser);
			table.setModel(repo.eachPersonTable(loggedUser));
			revenueField.setText(String.format("%.2f", loggedUser.getRevenue()));
			
		}
		
	}
	
	class ItemChangeListener implements ItemListener{

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
		          String item = e.getItem().toString();
		          selectedProduct = repo.SetSelectedProduct(item);
		          priceField.setText(Double.toString(selectedProduct.getPrice()));
		     }
		}
	}
	public void SetComboBox() {
		productsCombo.removeAllItems();
		String[] population = null;
		
		if(!(loggedUser==null)) {
			population = repo.SelectProducts();
			for(String str : population){
		    	productsCombo.addItem(str);
		    }
		}
	}
	
	public void Logout(Container container) {
		loggedUser = null;
		selectedProduct = null;
		for(Component c : container.getComponents()) {
			if(c instanceof JTextField) {
				  JTextField f = (JTextField) c;
				  f.setText("");
			} else if(c instanceof Container) {
				Logout((Container)c);
			}
			MyModel mm = (MyModel) table.getModel();
			
			while(mm.getRowCount() > 0) {
				mm.removeRow(0);
			}
			table.updateUI();
			productsCombo.removeAllItems();
			tabbedPane.setSelectedIndex(0);
		}
	}
	
	public void PopulateRevenue(User user) {
		if(user != null) {
			int a = user.getID();
			idField.setText(""+a);
			usernameField.setText(user.getUsername());
			nameField.setText(user.getFullName());
			revenueField.setText(user.getRevenue().toString());
		}
		table.setModel(repo.eachPersonTable(user));
	}


	 class AddAction implements ActionListener{
	
		@Override
		public void actionPerformed(ActionEvent e) {
			RegisterFrame regFrame = new RegisterFrame();
			regFrame.launch();
		}
		
	}
 
	 class LogInAction implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if(passwordLogInField.getText().length() > 0 && usernameLogInField.getText().length() > 0) {
				loggedUser = repo.GetFirstUserOrDefault(usernameLogInField.getText(), passwordLogInField.getText());
				if(loggedUser != null) {
					loggedUser.setRevenue(repo.CalculaterUserTotalRevenue(loggedUser));
					usernameLogInField.setText("");
					passwordLogInField.setText(""); 
					repo.infoBox("Log in successfull!", "Good Job!");
					PopulateRevenue(loggedUser);
					SetComboBox();
					tabbedPane.setSelectedIndex(1);
				}
				else {
					repo.infoBox("No such a user.","Nope");
				}
			}else {
				repo.infoBox("Please fill both fields!", "Oh come oon!");
			}
			
		}
		
	}
	
	 class AddProductAction implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(loggedUser == null) {
				repo.infoBox("Please Log In first", "Oops!");
				return;
			}
			AddProductFrame addProduct = new AddProductFrame();
			addProduct.launch();
		}
		 
	 }
	 class TableMouseClick extends MouseAdapter{

			@Override
			public void mouseClicked(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e)) {
					MyPopUp menu = new MyPopUp();
					menu.show(table, e.getX(), e.getY());
					
				}
			}
	 }
}













