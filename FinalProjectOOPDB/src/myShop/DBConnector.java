package myShop;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnector {
	
	static Properties prop = new Properties();
	
	static String url;         //= "jdbc:sqlserver://DESKTOP-MECR15I:1433;databaseName=shop";
	static String user = "ioandb";
	static String password = "ioandb";
	
	private static Connection conn = null; 
	
	public static Connection getConnection() {
		
		try {
			prop.load(new FileInputStream("D:\\Java projects\\FinalProjectOOPDB\\src\\myShop\\FileConfig\\conn.cfg"));
			
			url = prop.getProperty("DBurl");
			conn = DriverManager.getConnection(url,user,password);
			System.out.println("Connected Successfully!!");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Something went wrong, couldn't connect to SQL");
			e.printStackTrace();
		}
		return conn;
	}
}
