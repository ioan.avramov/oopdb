package myShop;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class RegisterFrame {

	private JFrame Register;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JTextField uNameField;
	private JTextField lNameField;
	private JTextField fNameField;

	/**
	 * Launch the application.
	 */
	public void launch() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterFrame window = new RegisterFrame();
					window.Register.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegisterFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Register = new JFrame();
		Register.setTitle("Register");
		Register.setBounds(100, 100, 360, 265);
		Register.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		Register.getContentPane().setLayout(null);
		
		JLabel reg_FirstName = new JLabel("First Name");
		reg_FirstName.setBounds(21, 23, 68, 23);
		Register.getContentPane().add(reg_FirstName);
		
		JLabel reg_LastName = new JLabel("Last Name");
		reg_LastName.setBounds(21, 57, 68, 23);
		Register.getContentPane().add(reg_LastName);
		
		JLabel reg_Username = new JLabel("Username");
		reg_Username.setBounds(21, 90, 68, 23);
		Register.getContentPane().add(reg_Username);
		
		JLabel reg_Password = new JLabel("Password");
		reg_Password.setBounds(21, 124, 68, 23);
		Register.getContentPane().add(reg_Password);
		
		JLabel reg_passwordConf = new JLabel("Confirm Password");
		reg_passwordConf.setBounds(21, 158, 94, 23);
		Register.getContentPane().add(reg_passwordConf);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(124, 125, 200, 20);
		Register.getContentPane().add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(125, 159, 200, 20);
		Register.getContentPane().add(passwordField_1);
		
		uNameField = new JTextField();
		uNameField.setBounds(124, 91, 200, 20);
		Register.getContentPane().add(uNameField);
		uNameField.setColumns(10);
		
		lNameField = new JTextField();
		lNameField.setColumns(10);
		lNameField.setBounds(124, 58, 200, 20);
		Register.getContentPane().add(lNameField);
		
		fNameField = new JTextField();
		fNameField.setColumns(10);
		fNameField.setBounds(124, 24, 200, 20);
		Register.getContentPane().add(fNameField);
		
		JButton reg_Register = new JButton("Register");
		reg_Register.setBounds(235, 190, 89, 23);
		Register.getContentPane().add(reg_Register);
		reg_Register.addActionListener(new Action());
		
	}
	 class Action implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			BaseRepository baseRepo = new BaseRepository();
			
			if(fNameField.getText().length() > 0 && lNameField.getText().length() > 0  && uNameField.getText().length() > 0 && passwordField.getText().length() > 0 ) {
				if(passwordField.getText().equals(passwordField_1.getText())) {
					User u = baseRepo.GetFirstUserOrDefault(uNameField.getText(), passwordField.getText());
					if(u == null) {
						baseRepo.InsertUser(fNameField.getText(), lNameField.getText(), uNameField.getText(), passwordField_1.getText());
						return;
					}
					else {
						baseRepo.infoBox("Such user already exists", "Sorry!");
						passwordField.setText("");
						passwordField_1.setText("");
						return;
					}
					
				}	
					baseRepo.infoBox("Passwords don't match!", "Oops!");
					passwordField.setText("");
					passwordField_1.setText("");
			}
			else {
				baseRepo.infoBox("Please enter all the fields.", "Don't be sneaky!");
			}
			
			
			
		}
		
	}
}
