package myShop;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class BaseRepository {
	
	Connection conn = null;
	ResultSet result;
	PreparedStatement state = null;
	Statement st;
	private ArrayList<Object> data=new ArrayList<Object>();
	
	public MyModel eachPersonTable(User user) {
		
		String sql = "Select p.Name, p.Price, Count(s.SellId) as 'Count' "+
					 "From Product p "+
					 "INNER JOIN Sold as s On s.ProductId = p.Id "+
					 "Where s.SellerID = "+user.getID()+
					 " Group by p.Name, p.Price ";
		
		conn = DBConnector.getConnection();
		MyModel model = null;
		ResultSet result = null;
		
		try {
			PreparedStatement state = conn.prepareStatement(sql);
			result = state.executeQuery();
			model = new MyModel(result);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return model;
	}
	
	public void DeleteSell(int prod_id, int sellCount) {
		String sql = "Delete TOP ("+sellCount+") from Sold Where ProductID = "+prod_id+"";
		
		conn = DBConnector.getConnection();
		
		try {
			st.execute(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int GetItemId(String name) {
		int id = 0;
		String sql = "SELECT TOP 1 Id FROM Product where Name ='"+name+"'";
		conn = DBConnector.getConnection();
		
		try {
			st = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			result = st.executeQuery(sql);
			if(result.first()) {
				id = result.getInt("Id");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	
	public BigDecimal CalculaterUserTotalRevenue(User user) {
		data = new ArrayList<Object>();
		String sql = "Select p.Price, Count(s.SellId) as 'Count' "+
					 "From Product p " +
					 "INNER JOIN Sold as s On s.ProductId = p.Id "+
					 "Where s.SellerID = " +user.getID()+
					 " Group by p.Name, p.Price";
		
		conn = DBConnector.getConnection();
		try {
			st = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			result = st.executeQuery(sql);
			
			 ResultSetMetaData metaData=result.getMetaData();
			 
			 int columnCount = metaData.getColumnCount();
			 
			 while(result.next()){
				 Object[] row=new Object[columnCount];
				 
				 for(int j=0;j<columnCount;j++){
					 
					 row[j]=result.getObject(j+1);
				 }		
				 data.add(row);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BigDecimal rev = new BigDecimal(0);
		
		for(int i = 0; i<data.size(); i++) {
			Object[] row = (Object[])data.get(i); 
			BigDecimal a = (BigDecimal)row[0];
			int b = (int)row[1];
			a = a.multiply(BigDecimal.valueOf(b));
		    rev = rev.add(a);
		}
		return rev;
	} 
	public void InsertUser(String FirstName, String LastName, String Username, String Password) {
		
		conn = DBConnector.getConnection();
		String sql = "insert into Users values(?,?,?,?);";
		
		try {
			state = conn.prepareStatement(sql);
			state.setString(1, FirstName);
			state.setString(2, LastName);
			state.setString(3, Username);
			state.setString(4, Password);
			
			state.execute();
			infoBox("User successfully added", "Nice!");
			
		} catch (SQLException e) {
			infoBox("Something went wrong", "Oops!");
		}
	}
	
	public void InsertProduct(String Name, float price){
		conn = DBConnector.getConnection();
		String sql = "insert into product values(?,?);";
		
		try {
			state = conn.prepareStatement(sql);
			state.setString(1, Name);
			state.setFloat(2, price);
			
			state.execute();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void UpdateProduct(Product product, String name, double price) {
		conn = DBConnector.getConnection();
		String sql = "UPDATE Product " +
					"SET Name= '"+name+"', Price= '"+price+"' " + 
					"Where Id =" + product.getID()+"";
		try {
			state = conn.prepareStatement(sql);
			state.execute();
			conn.close();
			infoBox("Product Updated", "Good Job.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}

	public void UpdateUser(User u, String fname, String lname, String username, String password) {
		conn = DBConnector.getConnection();
		String sql = "UPDATE Users " + 
				     "SET Fname= '"+fname+"', Lname= '"+lname+"' , Username = '"+username+"', Password = '"+password+"' " + 
				     "WHERE Id = "+u.getID()+"";
		try {
			state = conn.prepareStatement(sql);
			state.execute();
			conn.close();

			infoBox("User Updated", "Good Job.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void infoBox(String message, String titleBar) {
		
		JOptionPane.showMessageDialog(null, message, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public User GetFirstUserOrDefault(String username, String password) {
		
		String sql = "SELECT TOP 1 * FROM Users where Username = '"+username+"' and Password = '"+password+"'";
		conn = DBConnector.getConnection();
		User u = null;
		try {
			st = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			result = st.executeQuery(sql);
			
			if(result.first())
			{
				u = new User();
				u.setID(result.getInt("Id"));
				u.setFullName(result.getString("Fname") + " " +result.getString("Lname"));
				u.setUsername(result.getString("Username"));
				u.setFirstName(result.getString("Fname"));
				u.setLastName(result.getString("Lname"));
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return u;
	}
	public String[] SelectProducts() {
		
        ArrayList<String> prods = new ArrayList<String>();
        conn = DBConnector.getConnection();
        String sql = "SELECT [NAME] FROM PRODUCT";
        
        try {
            st = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
            result = st.executeQuery(sql);

            while(result.next())
            {
                prods.add(result.getString("NAME")); 
            }
            conn.close();
        } catch (Exception e) {

            e.printStackTrace();
        }
        String[] arr = new String[prods.size()];
        for(int i = 0; i < arr.length; i++) {
        	arr[i] = prods.get(i);
        }
        return arr;
    }
	public Product SetSelectedProduct(String selectedItem) {
		Product p = new Product();
		String sql = "SELECT TOP 1 * FROM Product Where Product.Name = '"+selectedItem + "'";
		conn = DBConnector.getConnection();
		 try {
	            st = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	            result = st.executeQuery(sql);
	            if(result.first()) {
	            	 p.setID(result.getInt("Id"));
	            	 p.setName(result.getString("Name"));
	            	 p.setPrice(result.getDouble("Price"));
	            }
	            conn.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		 return p;
   }
	
	public void DeleteUser(User u ) {
		String sql = "Delete from Sold Where SellerID = "+u.getID()+
					 " Delete from Users where Id = "+u.getID();
		
		conn = DBConnector.getConnection();
		try {
			state = conn.prepareStatement(sql);
			state.execute();
			infoBox("User Deleted!", "OOOhh!");
			conn.close();
		} catch (SQLException e) {
			infoBox("Shiieeet!", "Ohhh...");
			e.printStackTrace();
		}
	}
	public void SellItem(Product product, int quantity, User user) {
		
		String sql = "Insert into [Sold] (SellerID, ProductID) values ("+user.getID()+","+product.getID()+")";
		conn = DBConnector.getConnection();
			try {
				state = conn.prepareStatement(sql);
				for(int i = 0; i<quantity; i++) {
				state.execute();
				}
				infoBox("Item Sold!", "Hooray!");
				conn.close();
			} catch (SQLException e) {
				infoBox("Shiieeet!", "Ohhh...");
				e.printStackTrace();
			}
			BigDecimal new_Revenue = user.getRevenue();
			double addition = product.getPrice()*quantity;
			new_Revenue = new_Revenue.add(BigDecimal.valueOf(addition));
			user.setRevenue(new_Revenue);
	}
	
	public void DeleteProduct(Product p) {
        String sql = "Delete from Sold Where ProductID = "+p.getID()+
                     " Delete from Product where Id = "+p.getID();
        
        conn = DBConnector.getConnection();
        try {
            state = conn.prepareStatement(sql);
            state.execute();
            infoBox("Product Deleted!", "OOOhh!");
            conn.close();
        } catch (SQLException e) {
            infoBox("Shiieeet!", "Ohhh...");
            e.printStackTrace();
        }
    }
	
	
	
	
	
	
	
	
	
	
	
}
