package myShop;

import java.math.BigDecimal;

public class User {
	
	private int ID;
	private String FullName;
	private String FirstName;
	private String LastName;
	private String Username;
	private BigDecimal Revenue = new BigDecimal(0);;
	
	
	
	public int getID() {
		return ID; 
	}
	public void setID(int iD) {
		ID = iD;
	}
	
	
	public String getFullName() {
		return FullName;
	}
	public void setFullName(String fullName) {
		FullName = fullName;
	}
	
	
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	
	
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	
	
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	
	
	public BigDecimal getRevenue() {
		return Revenue; 
	}
	public void setRevenue(BigDecimal revenue) {
		Revenue = revenue;
	}
}
